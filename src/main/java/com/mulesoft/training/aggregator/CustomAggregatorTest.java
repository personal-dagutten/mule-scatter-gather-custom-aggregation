package com.mulesoft.training.aggregator;


import java.util.Iterator;
import java.util.List;

import org.mule.DefaultMuleEvent;
import org.mule.api.MuleContext;
import org.mule.api.MuleEvent;
import org.mule.api.MuleException;
import org.mule.api.context.MuleContextAware;
import org.mule.api.routing.AggregationContext;
import org.mule.api.routing.RouterResultsHandler;
import org.mule.routing.AggregationStrategy;
import org.mule.routing.DefaultRouterResultsHandler;

public class CustomAggregatorTest implements AggregationStrategy, MuleContextAware {

	private RouterResultsHandler resultsHandler = new DefaultRouterResultsHandler();
	private MuleContext originalMuleContenxt;
	
	@Override
	public MuleEvent aggregate(AggregationContext context) throws MuleException {
		
		List<MuleEvent> listEvents = context.collectEventsWithoutExceptions();
		
		MuleEvent manualEvent = DefaultMuleEvent.copy(context.getEvents().get(0));
		manualEvent.getMessage().setPayload("why am I here?");
		
		modifyListEvents(listEvents,manualEvent);
		
	
		return resultsHandler.aggregateResults(listEvents, context.getOriginalEvent(), originalMuleContenxt);
//		return manualEvent;
	}

	
	@Override
	public void setMuleContext(MuleContext context) {
		this.originalMuleContenxt = context;
	}
	
	
	private synchronized void modifyListEvents(List<MuleEvent> listEvents, MuleEvent manualEvent){
		
		for (Iterator<MuleEvent> iterator = listEvents.iterator(); iterator.hasNext(); ) {
			
			MuleEvent iteratorEvent = iterator.next();
			
			if(iteratorEvent.getMessage().getPayload().toString().contains("key1")){
				iterator.remove();
				
			}
			
		}
		
		listEvents.add(manualEvent);
		
	}
	
}